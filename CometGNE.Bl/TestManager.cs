﻿using CometGNE.Interfaces.Bl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CometGNE.Models;
using CometGNE.Interfaces.DAL;

namespace CometGNE.Bl
{
    public class TestManager : ITestManager
    {
        private readonly ITestRepository _testRepository;

        public TestManager(ITestRepository testRepository)
        {
            _testRepository = testRepository;
        }

        public IEnumerable<TestDto> GetAll()
        {
            return _testRepository.GetAll();
        }
    }
}
