﻿using CometGNE.Interfaces.Contracts;
using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;

namespace CometGNE.Composition.Registrations.Routes
{
    public static class TestRoutes
    {
        private const string BASE_URL = "Comet/api";

        public static void Register(ServiceHostFactory factory)
        {
            RouteTable.Routes.Add(new ServiceRoute(string.Format("{0}/test", BASE_URL), factory, typeof(ITestContract)));
        }
    }
}
