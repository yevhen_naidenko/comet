﻿using Autofac;
using Autofac.Integration.Wcf;
using Autofac.Integration.WebApi;
using CometGNE.Composition.Registrations.Components;
using CometGNE.Composition.Registrations.Routes;
using CometGNE.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace CometGNE.Composition
{
    public class DependencyResolver
    {
        public static void Resolve()
        {
            var factory = new AutofacServiceHostFactory();

            var builder = new ContainerBuilder();

            /*
             * 
             * REGISTER APP ROUTES SECTION
             * 
             */

            TestRoutes.Register(factory);

            /*
             * 
             * REGISTER APP DEPENDENCIES SECTION
             * 
             */

            builder.RegisterModule<DALRegistration>();
            builder.RegisterModule<BlRegistration>();
            builder.RegisterModule<ContractRegistration>();

            var container = builder.Build();

            AutofacHostFactory.Container = container;
        }

        public static IDependencyResolver ResolveWebApi()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<DALRegistration>();
            builder.RegisterModule<BlRegistration>();
            builder.RegisterModule<ContractRegistration>();

            builder.RegisterApiControllers(typeof(TestController).Assembly);

            var container = builder.Build();

            return new AutofacWebApiDependencyResolver(container);
        }
    }
}
