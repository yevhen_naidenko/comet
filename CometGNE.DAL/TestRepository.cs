﻿using CometGNE.Interfaces.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CometGNE.Models;

namespace CometGNE.DAL
{
    public class TestRepository : ITestRepository
    {
        public void Create(TestDto entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(TestDto entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TestDto> GetAll()
        {
            return new List<TestDto> { new TestDto { TestProperty = "Test1" }, new TestDto { TestProperty = "Test2" } };
        }

        public TestDto GetBy(Guid id)
        {
            throw new NotImplementedException();
        }

        public void Update(TestDto entity)
        {
            throw new NotImplementedException();
        }
    }
}
