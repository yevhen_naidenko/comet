﻿using CometGNE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CometGNE.Interfaces.Bl
{
    public interface ITestManager
    {
        IEnumerable<TestDto> GetAll();
    }
}
