﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using CometGNE.Interfaces.DAL;
using System.Collections.Generic;
using CometGNE.Models;
using CometGNE.Bl;
using System.Linq;
using CometGNE.Interfaces.Bl;

namespace CometGNE.Tests
{
    [TestClass]
    public class TestManagerTests
    {
        private ITestManager _testManager;

        [TestInitialize]
        public void Init()
        {
            var repositoryMock = new Mock<ITestRepository>();

            repositoryMock.Setup(x => x.GetAll()).Returns(new List<TestDto>
            {
                new TestDto { TestProperty = "test3" },
                new TestDto { TestProperty = "test4" },
                new TestDto { TestProperty = "test5" }
            });

            _testManager = new TestManager(repositoryMock.Object);
        }

        [TestMethod]
        public void TestMethod1()
        {
            // arrenge

            var expected = 3;

            // act
            var actual = _testManager.GetAll().ToList().Count;

            // assert
            Assert.AreEqual(expected, actual);

        }
    }
}
