﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CometGNE.Models
{
    [DataContract]
    public class ResponseModel<T>
    {
        public ResponseModel()
        {
        }

        public ResponseModel(T content)
        {
            Content = content;
            IsSucceeded = true;
        }

        public ResponseModel(string errorMessage)
        {
            Message = errorMessage;
            IsSucceeded = false;
        }

        [DataMember(Name = "content")]
        public T Content { get; set; }

        [DataMember(Name = "issucceeded")]
        public bool IsSucceeded { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }
    }
}
