﻿using Autofac;
using CometGNE.DAL;
using CometGNE.Interfaces.DAL;
using CometGNE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CometGNE.Composition.Registrations.Components
{
    public class DALRegistration : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TestRepository>().As<ITestRepository>();
        }
    }
}
