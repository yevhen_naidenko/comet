﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CometGNE.Interfaces.DAL
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();

        T GetBy(Guid id);

        void Create(T entity);

        void Update(T entity);

        void Delete(T entity);
    }
}
