﻿using CometGNE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace CometGNE.Interfaces.Contracts
{
    [ServiceContract]
    public interface ITestContract
    {
        [OperationContract]
        [WebGet(UriTemplate = "/get/all", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ResponseModel<IEnumerable<TestDto>> GetAll();

        [OperationContract]
        [WebGet(UriTemplate = "/get?id={id}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ResponseModel<TestDto> Get(int id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/save", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        void Post(string value);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "/update", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        void Put(int id, string value);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "/delete", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        void Delete(int id);
    }
}
