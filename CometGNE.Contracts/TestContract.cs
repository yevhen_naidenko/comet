﻿using CometGNE.Interfaces.Bl;
using CometGNE.Interfaces.Contracts;
using CometGNE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CometGNE.Contracts
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Single)]
    public class TestContract : ITestContract
    {
        private readonly ITestManager _testManager;

        public TestContract(ITestManager testManager)
        {
            _testManager = testManager;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public ResponseModel<IEnumerable<TestDto>> GetAll()
        {
            try
            {
                var result = _testManager.GetAll();

                return new ResponseModel<IEnumerable<TestDto>>(result);
            }
            catch(Exception ex)
            {
                // log the exception

                return new ResponseModel<IEnumerable<TestDto>>(ex.Message);
            }
        }

        public ResponseModel<TestDto> Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Post(string value)
        {
            throw new NotImplementedException();
        }

        public void Put(int id, string value)
        {
            throw new NotImplementedException();
        }
    }
}
