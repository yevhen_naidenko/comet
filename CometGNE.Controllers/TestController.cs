﻿using CometGNE.Interfaces.Bl;
using CometGNE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace CometGNE.Controllers
{
    public class TestController : ApiController
    {
        private readonly ITestManager _testManager;

        public TestController(ITestManager testManager)
        {
            _testManager = testManager;
        }

        // GET: api/Test
        public ResponseModel<IEnumerable<TestDto>> Get()
        {
            try
            {
                var result = _testManager.GetAll();

                return new ResponseModel<IEnumerable<TestDto>>(result);
            }
            catch (Exception ex)
            {
                // log the exception

                return new ResponseModel<IEnumerable<TestDto>>(ex.Message);
            }
        }

        // GET: api/Test/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Test
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Test/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Test/5
        public void Delete(int id)
        {
        }
    }
}
