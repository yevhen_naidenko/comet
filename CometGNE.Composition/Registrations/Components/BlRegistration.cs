﻿using Autofac;
using CometGNE.Bl;
using CometGNE.Interfaces.Bl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CometGNE.Composition.Registrations.Components
{
    public class BlRegistration : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TestManager>().As<ITestManager>();
        }
    }
}
