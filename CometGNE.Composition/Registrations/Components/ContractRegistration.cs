﻿using Autofac;
using CometGNE.Contracts;
using CometGNE.Interfaces.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CometGNE.Composition.Registrations.Components
{
    public class ContractRegistration : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TestContract>().As<ITestContract>();
        }
    }
}
