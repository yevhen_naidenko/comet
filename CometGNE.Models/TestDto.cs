﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CometGNE.Models
{
    [DataContract]
    public class TestDto
    {
        [DataMember(Name = "testprop")]
        public string TestProperty { get; set; } 
    }
}
